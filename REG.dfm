object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 411
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 32
    Width = 169
    Height = 289
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 432
    Top = 32
    Width = 169
    Height = 289
    Lines.Strings = (
      'Memo2')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 264
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 264
    Top = 103
    Width = 75
    Height = 25
    Caption = 'Containst'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 264
    Top = 142
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 264
    Top = 181
    Width = 75
    Height = 25
    Caption = 'LowerCase'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 264
    Top = 220
    Width = 75
    Height = 25
    Caption = 'UpperCase'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 264
    Top = 259
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = Button6Click
  end
end
